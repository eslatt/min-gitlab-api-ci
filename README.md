### goal

demonstrate pipelined regression test of gitlab api code (both authenticated an non authenticated api access) as simply as possible

#### non authenticated api access

```
curl -X GET https://gitlab.com/api/v4/projects
```

##### authenticated api acess

```
curl -X POST -F token=TOKEN -F ref=REF_NAME https://gitlab.com/api/v4/projects/22498908/trigger/pipeline
```

